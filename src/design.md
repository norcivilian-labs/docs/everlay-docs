# Design

web widget for alternative data stream representation

competes: dune analytics, grafana

interacts: with the website that integrates it

constitutes: a widget injected into a web application

includes: source, device, view, maps

patterns: message broker, flow-based programming, mva, mvvm

resembles: userway, qjackctl, opentelemetry.io

stakeholders: fetsorn

a library that exposes the data structure in a common application, much like an opentelemetry tracer support, not as hard for me as a browser extension that rewrites code, and not as hard for the developer as an ecosystem of products developed specifically for everlay 
web application can now expose business logic to arbitrary views instead of hiding it behind a single coupled view
the developer adds support and the site has support for data presentation, opt-in on the developer's side
developer mports the library and writes two functions, functions are compliant with the csvs standard - a database schema and an array of data records
for each tick there is an array of fresh new records, in the record all scalar quantities that describe the operation of the application
turns the internal logic of any application into a database
map needs to ignore the original relationships hard enough to achieve the a-ha moment of emergent behaviour, value array from device finding a new relationship in the sight, but preserve the metadata about the original relationship enough to achieve the a-ha moment of displaying the sight "as it should", but not rely on that metadata too much to avoid tight coupling of device and sight like in game engines.
map number from device to the modulus it is congruent to in the sight
 - mod10 to mod2, check if even or odd
 - strings to a mod10, parse "0x01" as 1, "100" as 100
 - string to hash string
 - hash string a mod10
 - string to mod2, string->hash->mod10->mod2
 - string to mod2, check if palindrome
a device website has a widget, inside the widget a dropdown with sights, list of options: "chart", "game", "pepper", "sounds", a map of the relationships between the device database schema and the view with the ability to edit manually or map with an algorithm, not a rigid coupling "show first number as a chart", but a mapping algorithm "expect a list of numbers and choose one for the chart" and a slider with the ability to roll back time.

there was an earlier prototype [repo](https://github.com/fetsorn/everlay) that connected sources of data, devices that transformed the data, and views that showed data, through a common map of handles.
devices were done in pure js and wasm
the mappping system did not have a matching algorithm and did not differentiate data types or modulo types of connectors
There were cases when the device needs to know something about the previous state of the device, or about the viewing state, and it cannot do this, it can only pull the source.
scalar values that were passed to representation could differ from the values that store the game state. and differ from intermediary state of representation.

## ideas
everlair - marketplace, browser extension with a list of views. like steam workshop, redmine marketplace.
