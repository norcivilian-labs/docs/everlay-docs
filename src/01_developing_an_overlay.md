# Developing an Overlay

Overlays are chosen by the user to show the data series in the app. If you want to develop your own overlay, you need to initialize an instance of `Overlay` with a data structure map and a function that resolves data arguments to HTML.

And initialize it somewhere in your web app.
```js
import {Widget, Overlay} from "@fetsorn/everlay"

const overlay = new Overlay((isTrue) => {
    return isTrue ? '<p>tick</p>' : '<p>tock</p>'
});

const map = ["mod2"]

const widget = new Widget();

widget.addOverlay({overlay, map})

await widget.init();
```

This overlay will show either "tick" or "tock" depending on whether the data coming from source resolves as 0 or 1 in modulo base 2.

Learn more about extending everlay in the other [User Guides](./user_guides).
