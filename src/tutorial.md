# Tutorial

To setup the widget you need to provide it with sources of data coming from your app. Each source should be an instance of `Source` with a getter function that returns a snapshot of data on each call, and a map of the data structure. 

And initialize it somewhere in your web app.
```js
import {Widget, Source} from "@fetsorn/everlay"

const source = new Source(() => {
    return Math.round(Math.random())
});

const map = ["mod2"]

const widget = new Widget();

widget.addSource({source, map})

await widget.init();
```

The widget is initialized with a graph overlay by default, and this will show a step function, ranging between 0 and 1.

Learn more about configuring the widget in the [User Guides](./user_guides).
