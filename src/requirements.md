# Requirements

- user must see data series as arbitrary overviews
- user should run in browser
- developer must opt in in web app code
- developer should have complete common algebra for all data series
- user should read overview from overlay, data source, mapping and device components
- user should switch overlay
- user should switch device
- user should switch data source
- user should map overlay and data source with a mapping
- developer should add overlay
- developer should connect data source
- developer should specify data source and a map outliet
- developer should specify overlay and a map inlet
- developer should add device
- developer should specify device, inlet and outlet maps
- user should scroll through all ticks of the data series
- user can input data in the web app to change data in the widget, for a game loop

- user can see data as graph
- user can see data as music
  - piano keyboard with five keys. Input is number, output is sound.
- user can see data as roguelike map
- user can see data as system dynamics
- user can see data as text legends
- user can see data as colours
- user can install custom sights from a browser extension
- developer can specify custom sights on widget initialization
- developer can specify a demo app for a sight
