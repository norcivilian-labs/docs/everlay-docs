## sights
 - colour sight
   - colour filling the screen, input is number, output is changing hue

 - chart sight
   - chart with five ordinate maximum and ticks on the coordinateaxis. input is number, output is curve on a graph.

 - roguelike sight
   - character in a game walks up, right, down, left, or stands - five options. Input is a number, and output is direction of movement.
   - character has five lives. Input is number, output is health change.
   - character chooses one of five response options in a dialogue. Input is number, output is dialogue change.
   - character chooses whether to walk, shoot, talk, jump or reload. Input is number, output is action type.
   - water fills the grid, each cell can be from one to five water depth. Input is number, output is flood.
   - car speed ranges from one to five. Input is number, output is acceleration or deceleration.
   - in a game a slice of random access memory shows a snapshot of numbers that vary in function
   - map modulo base 9 to movement of a dot on the screen, when data comes from the device, sight turns it to a number in the range from one to nine, and an actor in the sight moves left, right, or in one of the nine directions.
   - map clock, actor moves left or right in a tick
   - map random number, the agent moves randomly
   - this happens every tick and so a game plays itself, like dwarf fortress
   - movement of a dot along a line, module 2
   - add dimensions one at a time flatworld-like
   - identification is modulo 1 NFTs, unique IDs, there can be only one of them
   - a button "switch device" turns everything into modulo five instead of nine, and the actor no longer moves diagonally
   - sight acts as a game engine and device as game data
   - in a 2d game where a character can run left, right and jump, modulo base 3
   - in a 2d game like snake, which can go up, down, left and right, modulo base 4
   - in a 2d game, which can go up, down, left and right, and stop, modulo base 5
   - in a 2d game, which can go up, down, left and right, and stop, and diagonally modulo base 9
   - map modulo base eight to water flow levels

 - legends sight
   - logs like dwarf fortress legends

