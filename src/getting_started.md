# Getting Started

To integrate the widget in your app, install it as a project dependency
```sh
npm install @fetsorn/everlay 
```

And initialize it somewhere in your web app.
```js
import {Widget} from "@fetsorn/everlay"

await Widget.init()
```

Learn more about configuring the widget in the [Tutorial](./tutorial.md) and the [User Guides](./user_guides).
